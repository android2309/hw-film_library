package profile_module.hwfilm_library.models;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "roles", schema = "public")
@Getter
@Setter
@NoArgsConstructor
public class Role extends GenericModel{
    @Column(name = "title", nullable = false)
    private String title;
    @Column(name = "description")
    private String description;
}
