package profile_module.hwfilm_library.models;

public enum Genre {
    FANTASY,
    DRAMA,
    SCIENCE_FICTION,
    COMEDY
}
