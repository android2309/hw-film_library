package profile_module.hwfilm_library.models;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Table(name = "directors", schema = "public")
@Getter
@Setter
@NoArgsConstructor
public class Director extends GenericModel{
    @Column(name = "fio", nullable = false)
    private String fio;
    @Column(name = "position", nullable = false)
    private String position;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "film_directors",
            joinColumns = @JoinColumn(name = "director_id"), foreignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS"),
            inverseJoinColumns = @JoinColumn(name = "film_id"), inverseForeignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS")
    )
    private List<Film> films;
}
